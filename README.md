# Bank API

## Prerequites

This project uses docker and docker-compose

## Installation

### Clone project

```
git clone https://gitlab.com/atis/bank-api.git
```

### Set up environment and change passwords
```
cd bank-api
cp .env.example .env
nano .env
```

### Build containers
```
sudo docker-compose build
```

### Launch containers
```
sudo docker-compose up -d
```

### Install application framework 
```
sudo docker-compose exec app rm -rf vendor composer.lock
docker-compose exec app composer install
docker-compose exec app php artisan key:generate

```


### Create database
```
sudo docker-compose exec app php artisan migrate
```

### Seed with test data
```
sudo docker-compose exec app php artisan db:seed

```

## API description

### List client accounts
<details>
<summary><code>GET</code> <code><b>/api/client/{client_id}/accounts</b></code><code>(List accounts for client with {client_id})</code></summary>

#### Parameters
> | name    |  type      | data type      | description                                          |
> |---------|------------|----------------|------------------------------------------------------|
> | {client_id}|  required  | integer        | Client ID                                            |


#### Response

> | response code | content-type                      | response                                                            |
> |---------------|-----------------------------------|---------------------------------------------------------------------|
> | `200`         | `application/json`                | `{"code":"200","client":"Rico Farrell","accounts":[{"id":1,"client_id":1,"currency":"EUR","balance":"456.840","name":"Account 1","created_at":"2023-08-30T14:45:28.000000Z","updated_at":"2023-08-30T14:45:28.000000Z","deleted_at":null},{"id":2,"client_id":1,"currency":"EUR","balance":"37175.520","name":"Account 2","created_at":"2023-08-30T14:45:28.000000Z","updated_at":"2023-08-30T14:45:28.000000Z","deleted_at":null}]}`                |
> | `404`         | `application/json`                | `{"code":"400","message":"Client not found"}`                       |

##### Example cURL
```bash
curl -s http://127.0.0.1:8000/api/client/${CLIENT_ID}/accounts
```

</details>

### Transfer funds between two accounts

<details>
<summary><code>PUT</code> <code><b>/api/account/{account_from_id}/transfer/{account_to_id}/</b></code><code>(Transfer funds between {account_from_id} and {account_to_id})</code></summary>

#### Parameters

> | name        |  type      | data type      | description                                          |
> |-------------|------------|----------------|------------------------------------------------------|
> | `{account_from_id}`     |  required  | integer         | ID of account to transfer funds from              |
> | `{account_to_id}`     |  required  | integer         | ID of acocunt to transfer funds to              |
> | `"amount"`  |  required in body JSON  | string        | Amount of funds to transfer in form 1234.789 (up to 3 digit subunit)                                            |
> | `"currency"`|  required in body JSON  | string        | ISO 4217 currency code. Must match recipient account currency                                          |


#### Response

> | response code | content-type                      | response                                                            |
> |---------------|-----------------------------------|---------------------------------------------------------------------|
> | `200`         | `application/json`                | `{"code":"200","rate":0.091905,"amount":45.9525}`                         |
> | `400`         | `application/json`                | `{"code":"400","message":{"amount":["The amount field format is invalid."]}}`                       |
> | `401`         | `application/json`                | `{"code":"401","message":"Amount transfered should be in receiving account currency: SEK"}`                         |
> | `402`         | `application/json`                | `{"code":"402","message":"Error during transaction. Check if sufficient funds in account (tried to transfer 45952.5 USD )"}`                         |
> | `500`         | `application/json`                | `{"code":"500","message":"Error obtaining exchange rate"}`                         |


##### Example cURL
```bash
curl -s -X PUT --header 'Content-Type: application/json' http://127.0.0.1:8000/api/account/${ACCOUNT_FROM_ID}/transfer/${ACCOUNT_TO_ID}/ -d '{"amount":"10","currency":"EUR"}'
```

</details>

### List account transactions
<details>
<summary><code>GET</code> <code><b>/api/account/{account_id}/transactions?limit={limit}&offset={offset}</b></code><code>(List transactions for account {account_id})</code></summary>

#### Parameters
> | name    |  type      | data type      | description                                          |
> |---------|------------|----------------|------------------------------------------------------|
> | {account_id}|  required  | integer        | Account ID                                            |
> | {limit} |  optional query string argument   | integer        | Limit results to {limit} transactions. Default 10                                            |
> | {offset} | optional query string argument   | integer        | Limit results to {limit} transactions. Default 10                                            |


#### Response

> | response code | content-type                      | response                                                            |
> |---------------|-----------------------------------|---------------------------------------------------------------------|
> | `200`         | `application/json`                | `{"code":"200","transactions":[{"id":33,"time":"2023-08-30 15:33:10","name":"Miss Alyson Dickinson V","other_account_id":9,"sent_amount":"-45.953","sent_currency":"USD","rate":"0.09191","amount":"500.000","currency":"SEK"},{"id":31,"time":"2023-08-30 15:32:43","name":"Miss Alyson Dickinson V","other_account_id":9,"sent_amount":"-0.919","sent_currency":"USD","rate":"0.09191","amount":"10.000","currency":"SEK"}]}`                |
> | `404`         | `application/json`                | `{"code":"400","message":"Client not found"}`                       |

##### Example cURL
```bash
curl -s 'http://127.0.0.1:8000/api/account/${ACCOUNT_ID}/transactions?limit=3&offset=0'
```

</details>
