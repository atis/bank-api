<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('client/{client}/accounts', 'App\Http\Controllers\Api\AccountController@getAccounts');
Route::get('account/{account}/transactions', 'App\Http\Controllers\Api\TransactionController@getTransactions');

//Route::get('stats/question/average/{survey?}', 'App\Http\Controllers\Api\StatsController@getQuestionAverage');
Route::put('account/{from}/transfer/{to}/', 'App\Http\Controllers\Api\TransactionController@transfer');
