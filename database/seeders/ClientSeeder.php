<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Client;
use App\Models\Account;

class ClientSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $faker = Faker::create();
        foreach (range(1,100) as $index) {
            $client = Client::create([
                'name'=>$faker->name,
            ]);
            
            Account::create([
                'client_id' => $client->id,
                'name' => $faker->words(3,true),
                'currency' => $faker->randomElement(['EUR','USD','GBP']),
                'balance' => $faker->randomFloat(2,0,500)
            ]);

            Account::create([
                'client_id' => $client->id,
                'name' => $faker->words(3,true),
                'currency' => $faker->randomElement(['EUR','USD','GBP','PLN','UAH','SEK']),
                'balance' => $faker->randomFloat(2,0,50000)
            ]);
        }
    }
}
