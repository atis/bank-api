#!/bin/bash

ACCOUNT=9
ACCOUNT=8

function _format() {
    local in
    read in
    echo "$in" | jq
    if [ "$?" != "0" ]; then
        echo "$in" | tail -n100
    fi
}

#curl -s "http://127.0.0.1:8000/api/account/$ACCOUNT/transactions?limit=3" | _format

curl -s "http://127.0.0.1:8000/api/account/$ACCOUNT/transactions?limit=3" | _format
curl -s "http://127.0.0.1:8000/api/account/$ACCOUNT/transactions?limit=3&offset=3" | _format
curl -s "http://127.0.0.1:8000/api/account/$ACCOUNT/transactions?limit=3&offset=6" | _format

