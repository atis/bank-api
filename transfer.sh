#!/bin/bash

FROM=9
TO=8
AMOUNT=10
CURRENCY=USD
#CURRENCY=EUR

function _format() {
    local in
    read in
    echo "$in" | jq
    if [ "$?" != "0" ]; then
        echo "$in" | tail -n100
    fi
}

#for i in `seq 1 100`; do
    curl -s -X PUT --header "Content-Type: application/json" http://127.0.0.1:8000/api/account/$FROM/transfer/$TO/ -d '{"amount":"'$AMOUNT'","currency":"'$CURRENCY'"}' | _format
#done

