#!/bin/bash

CLIENT=1

FORMAT=1
function _format() {
    local in
    read in
    echo "$in" | jq
    if [ "$?" != "0" ]; then
        echo "$in" | tail -n100
    fi
}


curl -s "http://127.0.0.1:8000/api/client/$CLIENT/accounts" | _format

