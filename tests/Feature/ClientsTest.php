<?php

namespace Tests\Feature;

//use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;
use App\Models\Client;
use App\Models\Account;

class ClientsTest extends TestCase
{
    use WithFaker;
    var $client_id = 0;
    var $account_ids = [];

    public function setUp():void {
        parent::setUp();

        $client = Client::create([
            'name'=>$this->faker->name,
        ]);
        $this->client_id = $client->id;
            
        $acc1 = Account::create([
            'client_id' => $client->id,
            'name' => $this->faker->words(3,true),
            'currency' => 'EUR',
            'balance' => '100.00'
        ]);
        $this->account_ids[] = $acc1->id;

        $acc2 = Account::create([
            'client_id' => $client->id,
            'name' => $this->faker->words(3,true),
            'currency' => 'GBP',
            'balance' => '200.00'
        ]);
        $this->account_ids[] = $acc2->id;
    }

    public function tearDown():void {
        Account::where('client_id', $this->client_id)->delete();
        Client::where('id', $this->client_id)->delete();
        parent::tearDown();
    }

    /**
     * Test Client accounts
     */
    public function test_client_2_accounts(): void
    {
        $response = $this->getJson('/api/client/'.$this->client_id.'/accounts/');
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json
                ->where('code','200')
                ->whereType('accounts','array')
                ->has('accounts',2)
                ->has('accounts.0', fn (AssertableJson $json) =>
                    $json
                        ->where('currency','EUR')
                        ->where('balance','100.000')
                        ->where('client_id',$this->client_id)
                        ->has('created_at')
                        ->etc()
                )
                ->has('accounts.1', fn (AssertableJson $json) =>
                    $json
                        ->where('currency','GBP')
                        ->where('balance','200.000')
                        ->where('client_id',$this->client_id)
                        ->has('created_at')
                        ->etc()
                )
                ->etc()
        );
    }

    public function test_client_1_account(): void
    {
        /* Delete last account */
        $account_id = array_pop($this->account_ids);
        Account::where('id', $account_id)->delete();
        
        $response = $this->getJson('/api/client/'.$this->client_id.'/accounts/');
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json
                ->where('code','200')
                ->whereType('accounts','array')
                ->has('accounts',1)
                ->has('accounts.0', fn (AssertableJson $json) =>
                    $json
                        ->where('currency','EUR')
                        ->where('balance','100.000')
                        ->where('client_id',$this->client_id)
                        ->has('created_at')
                        ->etc()
                )
                ->etc()
        );
    }

    public function test_client_0_accounts(): void
    {
        /* Delete last account */
        $account_id = array_pop($this->account_ids);
        Account::where('id', $account_id)->delete();
        $account_id = array_pop($this->account_ids);
        Account::where('id', $account_id)->delete();
        
        $response = $this->getJson('/api/client/'.$this->client_id.'/accounts/');
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json
                ->where('code','200')
                ->whereType('accounts','array')
                ->has('accounts',0)
                ->etc()
        );
    }

    public function test_client_invalid(): void
    {
        $response = $this->getJson('/api/client/'.($this->client_id+100000).'/accounts/');
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json
                ->where('code','404')
                ->etc()
        );
    }

}
