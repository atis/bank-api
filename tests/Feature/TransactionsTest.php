<?php

namespace Tests\Feature;

//use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;
use App\Models\Client;
use App\Models\Account;
use App\Models\Transaction;

class TransactionsTest extends TestCase
{
    use WithFaker;
    var $client_ids = [];
    var $account_ids = [];

    public function setUp():void {
        parent::setUp();

        $client1 = Client::create([
            'name'=>$this->faker->name,
        ]);
        $this->client_ids[] = $client1->id;

        $client2 = Client::create([
            'name'=>$this->faker->name,
        ]);
        $this->client_ids[] = $client2->id;
            
        $acc1 = Account::create([
            'client_id' => $client1->id,
            'name' => $this->faker->words(3,true),
            'currency' => 'EUR',
            'balance' => '100.00'
        ]);
        $this->account_ids[] = $acc1->id;

        $acc2 = Account::create([
            'client_id' => $client2->id,
            'name' => $this->faker->words(3,true),
            'currency' => 'GBP',
            'balance' => '200.00'
        ]);
        $this->account_ids[] = $acc2->id;

        $acc3 = Account::create([
            'client_id' => $client2->id,
            'name' => $this->faker->words(3,true),
            'currency' => 'EUR',
            'balance' => '200.00'
        ]);
        $this->account_ids[] = $acc3->id;
    }

    public function tearDown():void {
        while (!empty($this->client_ids)) {
            $client_id = array_pop($this->client_ids);
            Account::where('client_id', $client_id)->delete();
            Client::where('id', $client_id)->delete();
        }
        parent::tearDown();
    }

    /**
     * Test transfering 100 EUR from EUR to GBP account
     */
    public function test_transfer_100eur_wrong_currency(): void
    {
        $response = $this->putJson('/api/account/'.$this->account_ids[0].'/transfer/'.$this->account_ids[1], ['amount'=>'100','currency'=>'EUR']);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json
                ->where('code','401')  /* Does not match target currency */
                ->etc()
        );
    }

    /**
     * Test transfering 10 GBP from EUR to GBP account
     */
    public function test_transfer_10gbp_to_gbp(): void
    {
        $response = $this->putJson('/api/account/'.$this->account_ids[0].'/transfer/'.$this->account_ids[1], ['amount'=>'10','currency'=>'GBP']);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json
                ->where('code','200')
                ->etc()
        );

        /* Test balance of account 2 */
        $acc2 = Account::find($this->account_ids[1]);
        $this->assertEquals($acc2->balance, '210.000');
    }

    /**
     * Test transfering 10 EUR from EUR to EUR account
     */
    public function test_transfer_10eur_to_eur(): void
    {
        $response = $this->putJson('/api/account/'.$this->account_ids[0].'/transfer/'.$this->account_ids[2], ['amount'=>'10','currency'=>'EUR']);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json
                ->where('code','200')
                ->etc()
        );

        /* Test balance of account 1 */
        $acc2 = Account::find($this->account_ids[0]);
        $this->assertEquals($acc2->balance, '90.000');

        /* Test balance of account 2 */
        $acc2 = Account::find($this->account_ids[2]);
        $this->assertEquals($acc2->balance, '210.000');
    }

    /**
     * Test transfering 100 EUR from EUR to EUR account
     */
    public function test_transfer_100eur_to_eur(): void
    {
        $response = $this->putJson('/api/account/'.$this->account_ids[0].'/transfer/'.$this->account_ids[2], ['amount'=>'100','currency'=>'EUR']);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json
                ->where('code','200')
                ->etc()
        );

        /* Test balance of account 1 */
        $acc2 = Account::find($this->account_ids[0]);
        $this->assertEquals($acc2->balance, '0.000');

        /* Test balance of account 2 */
        $acc2 = Account::find($this->account_ids[2]);
        $this->assertEquals($acc2->balance, '300.000');
    }

    /**
     * Test transfering 99.99 EUR from EUR to EUR account
     */
    public function test_transfer_99_99eur_to_eur(): void
    {
        $response = $this->putJson('/api/account/'.$this->account_ids[0].'/transfer/'.$this->account_ids[2], ['amount'=>'99.99','currency'=>'EUR']);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json
                ->where('code','200')
                ->etc()
        );

        /* Test balance of account 1 */
        $acc2 = Account::find($this->account_ids[0]);
        $this->assertEquals($acc2->balance, '0.010');

        /* Test balance of account 2 */
        $acc2 = Account::find($this->account_ids[2]);
        $this->assertEquals($acc2->balance, '299.990');
    }

    /**
     * Test negative amount
     */
    public function test_transfer_negative_amount(): void
    {
        $response = $this->putJson('/api/account/'.$this->account_ids[0].'/transfer/'.$this->account_ids[2], ['amount'=>'-100','currency'=>'EUR']);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json
                ->where('code','400')
                ->etc()
        );

        /* Test balance of account 1 */
        $acc2 = Account::find($this->account_ids[0]);
        $this->assertEquals($acc2->balance, '100.000');

        /* Test balance of account 2 */
        $acc2 = Account::find($this->account_ids[2]);
        $this->assertEquals($acc2->balance, '200.000');
    }

    /**
     * Test insufficient balance
     */
    public function test_transfer_insufficient_balance(): void
    {
        $response = $this->putJson('/api/account/'.$this->account_ids[0].'/transfer/'.$this->account_ids[2], ['amount'=>'101','currency'=>'EUR']);
        $response->assertStatus(200);
        $response->assertJson(fn (AssertableJson $json) =>
            $json
                ->where('code','402')
                ->etc()
        );

        /* Test balance of account 1 */
        $acc2 = Account::find($this->account_ids[0]);
        $this->assertEquals($acc2->balance, '100.000');

        /* Test balance of account 2 */
        $acc2 = Account::find($this->account_ids[2]);
        $this->assertEquals($acc2->balance, '200.000');
    }

    /**
     * Test transaction log
     */
    public function test_transaction_log_empty(): void
    {
        $response = $this->getJson('/api/account/'.$this->account_ids[0].'/transactions?limit=0&offset=0');
        $response->assertStatus(200);

        $response->assertJson(fn (AssertableJson $json) =>
            $json
                ->whereType('transactions','array')
                ->has('transactions',0)
                ->where('code','200')
                ->etc()
        );
    }

    /**
     * Test transaction log 
     */
    public function test_transaction_log(): void
    {
        $total_amount = 0;
        for ($i=0; $i<10; $i++) {
            $amount = round(($i+1)/100,2);
            $total_amount += $amount;
            
            $response = $this->putJson('/api/account/'.$this->account_ids[0].'/transfer/'.$this->account_ids[2], ['amount'=>$amount,'currency'=>'EUR']);
            $response->assertStatus(200);
            $response->assertJson(fn (AssertableJson $json) =>
                $json
                    ->where('code','200')
                    ->etc()
            );
        }

        /* Paginate each page */
        $count = 0;
        $limit = 3;
        $offset = 0;

        $items_per_page = [3,3,3,1];

        foreach ($items_per_page as $expected_count) {
            $response = $this->getJson('/api/account/'.$this->account_ids[0].'/transactions?limit='.$limit.'&offset='.$offset);
            $response->assertStatus(200);

            $response->assertJson(fn (AssertableJson $json) =>
                $json
                    ->whereType('transactions','array')
                    ->where('code','200')
                    ->has('transactions',$expected_count, fn(AssertableJson $json) =>
                        $json
                            ->has('id')
                            ->has('time')
                            ->has('name')
                            ->where('other_account_id',$this->account_ids[2])
                            ->has('sent_amount')
                            ->where('sent_currency','EUR')
                            ->where('rate','1.00000')
                            ->where('currency','EUR')
                            ->has('amount')
                            ->etc()
                    )
                    ->etc()
            );
            $count += count($response->json()['transactions']);
            $offset=$offset+$limit;
        }
        $this->assertEquals($count,$i);
    }
    
    



}
