<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Response;
use Illuminate\Http\Client\ConnectionException;


use App\Models\Account;
use App\Models\Journal;
use App\Models\Transaction;


class TransactionController extends Controller
{
    // Return list of transactions
    public function getTransactions(Request $request, int $account_id): JsonResponse {
        $limit=10;
        $offset=0;
        if($request->has('limit')) $limit=$request->limit;
        if($request->has('offset')) $offset=$request->offset;

        $result['code']='200';
        
        $account = Account::find($account_id);

        $result['transactions'] = Transaction::join('journals','journals.id', '=', 'transactions.journal_id')
            ->join('transactions AS other_transactions','other_transactions.journal_id','=','journals.id')
            ->join('accounts AS other_accounts','other_accounts.id','=','other_transactions.account_id')
            ->join('clients','clients.id','=','other_accounts.client_id')
            ->where('transactions.account_id', $account_id)
            ->where('other_transactions.account_id','<>',$account_id)
            ->orderBy('transactions.id', 'desc')->limit($limit)->offset($offset)
            ->get([
                'transactions.id',
                'journals.created_at AS time',

                /* Other account */
                'clients.name',
                'other_accounts.id AS other_account_id',
                'other_transactions.amount AS sent_amount',
                'other_accounts.currency AS sent_currency',

                'journals.rate',

                /* This account */
                'transactions.amount AS amount',
            ])->toArray();

        /* Add account currency */
        foreach ($result['transactions'] as $key => $value) {
            $result['transactions'][$key]['currency'] = $account->currency;
        }

        return response()->json($result);
    }
    
    /* Make a money transfer */
    public function transfer(Request $request, int $from, int $to): JsonResponse {
        /* Validate parameters */
        $validator = Validator::make([
            'amount' => $request->amount,
            'currency' => $request->currency
           ],[
            'amount' => 'required|regex:/^\d{1,7}(\.\d{1,3})?$/',
            'currency' => 'required|regex:/[A-Z]{3}/',
        ]);
        if ($validator->fails()) {
            return response()->json(['code' => '400', 'message' => $validator->errors()->get('*')]);
        }

        $from_account = Account::find($from);
        $to_account = Account::find($to);
        
        $rate = 1;
        $amount = $request->amount;
        $currency = $request->currency;
        
        if ($currency!=$to_account->currency) {
            /* Allow only currency of receiving account, as per requirements v1.1 item 3.3 */
            return response()->json(['code' => '401', 'message' => 'Amount transfered should be in receiving account currency: '.$to_account->currency]);
        }
        
        if ($from_account->currency!=$to_account->currency) {
            /* Obtain exchange rate */
            $url = 'https://api.exchangerate.host/latest?base='.$to_account->currency.'&symbols='.$from_account->currency.'';
            try {
                $response = Http::timeout(2)->get($url);
            } catch(\Exception $e) {
                return response()->json(['code' => '500', 'message' => 'Error obtaining exchange rate']);
            }

            if ($response->failed() || $response->serverError() || $response->clientError() || $response->json()['success']!='true') {
                return response()->json(['code' => '500', 'message' => 'Error obtaining exchange rate']);
            }
            $rate = $response->json()['rates'][$from_account->currency];
        }

        $from_amount = $amount*$rate;
        $from_currency = $from_account->currency;

        $result['code'] = '200';
        $result['rate'] = $rate;
        /* Begin transaction */
        try {
            $response = DB::transaction(function() use ($from, $to, $amount, $currency, $rate, $from_amount, $from_currency) {
                /* Reload From/To accounts, as balance could have changed */
                $from_account = Account::find($from);
                $to_account = Account::find($to);
            
                $journal = Journal::create(['rate'=>$rate]);
                $from_transaction = Transaction::create([
                    'journal_id' => $journal->id,
                    'account_id' => $from_account->id,
                    'amount' => -$from_amount,
                ]);
                $from_account->decrement('balance',$from_amount);
            
                $to_transaction = Transaction::create([
                    'journal_id' => $journal->id,
                    'account_id' => $to_account->id,
                    'amount' => $amount,
                ]);
                $to_account->increment('balance',$amount);
                return $from_amount;
            });
            /* End transaction */

            $result['amount'] = $response;
        } catch (\Exception $e) {
            return response()->json(['code' => '402', 'message' => 'Error during transaction. Check if sufficient funds in account (tried to transfer '.$from_amount.' '.$from_currency.' )']);
        }
        return response()->json($result);
    }
}
