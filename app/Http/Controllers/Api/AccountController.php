<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

use App\Models\Account;
use App\Models\Client;

class AccountController extends Controller
{
    //
    public function getAccounts(Request $request, int $client_id): JsonResponse {
        $client = Client::find($client_id);
        if (!$client) {
            return response()->json(['code' => '404', 'message' => 'Client not found']);
        }
        $result['code'] = '200';
        $result['client'] = $client->name;
        $result['accounts'] = Account::where(['client_id'=>$client_id])->get()->toArray();
        
        return response()->json($result);
    }
}
